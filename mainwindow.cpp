#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "client.h"
#include "settings.h"
#include "basic.h"
#include "advanced.h"
#include "crypto.h"
#include <QFileDialog>
#include "urldialog.h"
#include <QMessageBox>
#include <QNetworkProxy>
#include <QCloseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    portNum = 5000;
    useProxy = false;
    textColor = "#484848";
    serverColor = "#82b8ff";
    statusColor = "#8c8c8c";
    serverAddr = "localhost";
    nickName = "Nobody";
    crypt = new crypto();
    client = new Client();
    ui->mainText->setOpenExternalLinks(true);
    ui->textEdit->setFocus();
    this->create_tray_icon();
    this->setWindowTitle("Chat Client");
    this->setWindowIcon(QIcon("client.png"));
    QObject::connect(client, SIGNAL(dataReceived(QByteArray)), this, SLOT(display(QByteArray)));
    QObject::connect(client, SIGNAL(connected()), this, SLOT(connected()));
    QObject::connect(client, SIGNAL(notConnected()), this, SLOT(notConnected()));
    QObject::connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(onExit()));
    ui->mainText->append("<font color=\"" + statusColor + "\">Ready.</font>");
}

//connection functions

void MainWindow::connected()
{
    ui->mainText->append("<font color=\"" + statusColor + "\">Connected: " + serverAddr + ":" + QString::number(portNum) + "</font>");
    ui->sendButton->setEnabled(true);
}

void MainWindow::notConnected()
{
    ui->mainText->append("<font color=\"" + statusColor + "\">Connection Failed: " + client->getErrorCode() + ".</font>");
    ui->sendButton->setEnabled(false);
}

void MainWindow::setServer(QString s, int p)
{
    ui->mainText->append("<font color=\"" + statusColor + "\">Connecting: " + serverAddr + ":" + QString::number(portNum) + "</font>");
    ui->sendButton->setEnabled(false);
    client->disconnectServer();
    client->connectToHost(s, p);
}

//sending functions

void MainWindow::SendMsg(QString msg)
{
    QByteArray data = msg.toLocal8Bit().toBase64();
    QByteArray out = crypt->toEnc(data);
    client->writeData(out);
}

void MainWindow::on_sendButton_clicked()
{
    QString msg = ui->textEdit->text();
    msg = msg.prepend(nickName + ": ");
    SendMsg(msg);
    msg = msg.append("</font>").prepend("<font color=\"" + textColor + "\">");
    ui->mainText->append(msg);
    ui->textEdit->clear();
}

void MainWindow::sendURL(QString text, QString url)
{
    ui->textEdit->insert("<a href=\"" + url + "\">" + text + "</a>");
}

//displaying functions

void MainWindow::display(QByteArray data)
{
    QByteArray datab = QByteArray::fromBase64(data);
    QString msg = QString(datab);
    if (msg.indexOf("SRVMSG::", 0) == 0) {
        msg = msg.remove(0, 8).append("</font>").prepend("<font color=\"" + serverColor + "\">");
        ui->mainText->append(msg);
    } else {
        if (data.indexOf("CLIENTFILE::", 0) == 0) {
            data = data.remove(0, 12);
            int index = data.lastIndexOf("::FILENAME:");
            QString sentFileName = QByteArray(data.mid(index, data.size() - index)).replace("::FILENAME:", "");
            data = data.remove(index, data.size() - index);
            QString fileName = QFileDialog::getSaveFileName(NULL, tr("Save file"), sentFileName);
            if (fileName != "") {
                QFile file(fileName);
                if (file.open(QIODevice::WriteOnly)) {
                    file.write(data);
                    file.close();
                    ui->mainText->append("File saved: " + fileName);
                } else {
                    QMessageBox messageBox;
                    messageBox.critical(0, "Error", "Could not open file!");
                    messageBox.setFixedSize(500, 200);
                }
            }
        } else {
            QString msg = QString(QByteArray::fromBase64(crypt->toDec(data)));
            QString msgo = msg;
            msg.append("</font>").prepend("<font color=\"" + textColor + "\">");
            if (this->isMinimized() || this->isHidden()) {
                showMessage(msgo);
            }
            ui->mainText->append(msg);
        }
    }
}

void MainWindow::showMessage(QString msg)
{
    QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon();
    m_tray_icon->showMessage("", msg, icon, 1000);
}

//settings functions

void MainWindow::on_actionSettings2_triggered()
{
    QNetworkProxy cur = client->getProxy();
    settings *s;
    if (cur.hostName() != "") {
        s = new settings(nickName, portNum, serverAddr, textColor, serverColor, statusColor, useProxy, cur.hostName(), cur.port(), cur.user(), cur.password(), this);
    } else {
        s = new settings(nickName, portNum, serverAddr, textColor, serverColor, statusColor, useProxy, "127.0.0.1", 8080, "", "", this);
    }
    QObject::connect(s, SIGNAL(valueChanged(QString, QString, int, QString, QString, QString, bool,
                                            QString, int, QString, QString)), this, SLOT(valueChanged(QString, QString, int, QString
                                                    , QString, QString, bool, QString, int, QString, QString)));
    s->show();
}

void MainWindow::valueChanged(QString nickName, QString serverAddr, int portNum, QString chatColor, QString serverColor, QString statusColor, bool proxy, QString proxySrv, int proxyPort, QString proxyUser, QString proxyPass)
{
    this->nickName = nickName;
    this->textColor = chatColor;
    this->serverColor = serverColor;
    this->statusColor = statusColor;

    if (useProxy != proxy) {
        useProxy = proxy;
        if (proxy) {
            QNetworkProxy proxys;
            proxys.setType(QNetworkProxy::Socks5Proxy);
            proxys.setHostName(proxySrv);
            proxys.setPort(proxyPort);
            if (proxyUser != "") {
                proxys.setUser(proxyUser);
                proxys.setPassword(proxyPass);
            }
            client->setProxy(proxys);
        } else {
            client->disconnectServer();
            client->disableProxy();
        }
    }
    if (this->serverAddr != serverAddr || this->portNum != portNum) {
        this->serverAddr = serverAddr;
        this->portNum = portNum;
        setServer(serverAddr, portNum);
    }
}

//action functions

void MainWindow::on_actionDisconnect_triggered()
{
    client->disconnectServer();
    ui->mainText->append("<font color=\"" + statusColor + "\">Disconnected: " + serverAddr + ":" + QString::number(portNum) + "</font>");
    ui->sendButton->setEnabled(false);
}

void MainWindow::on_actionConnect_triggered()
{
    setServer(serverAddr, portNum);
}

void MainWindow::on_actionAdvanced_triggered()
{
    Advanced *a = new Advanced(crypt);
    a->show();
}

void MainWindow::on_actionBasic_triggered()
{
    Basic *b = new Basic();
    b->show();
}

void MainWindow::on_actionURL_triggered()
{
    URLDialog *d = new URLDialog();
    QObject::connect(d, SIGNAL(sendURL(QString, QString)), this, SLOT(sendURL(QString, QString)));
    d->show();
}

void MainWindow::on_actionFile_triggered()
{
    QFileDialog *f = new QFileDialog();
    QString file = f->getOpenFileName();
    if (file != "") {
        QFile *sendF = new QFile(file);
        QByteArray ba;
        if (sendF->open(QIODevice::ReadOnly)) {
            SendMsg(nickName + ": File: " + QFileInfo(sendF->fileName()).baseName() + "." + QFileInfo(sendF->fileName()).suffix());
            ba = sendF->readAll();
            ui->mainText->append(nickName + ": File: " + QFileInfo(sendF->fileName()).baseName() + "." + QFileInfo(sendF->fileName()).suffix());
            ba.prepend("CLIENTFILE::");
            ba.append("::FILENAME:" + QFileInfo(sendF->fileName()).baseName() + "." + QFileInfo(sendF->fileName()).suffix());
            client->writeData(ba);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

void MainWindow::on_actionImport_Keys_triggered()
{
    QFileDialog *f = new QFileDialog();
    QString file = f->getOpenFileName();
    if (file != "") {
        QFile *keyF = new QFile(file);
        if (keyF->open(QIODevice::ReadOnly)) {
            QDataStream in(keyF);
            QHash<QString, QVariant> keyset;
            in >> keyset;
            crypt->setUsedKeys(keyset);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

void MainWindow::on_actionExport_Keys_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(NULL, tr("Save file"), "client.keys");
    if (fileName != "") {
        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly)) {
            QDataStream out(&file);
            out << crypt->getUsedKeys();
            file.flush();
            file.close();
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

//system tray icon

void MainWindow::create_tray_icon()
{
    m_tray_icon = new QSystemTrayIcon(QIcon("client.png"), this);
    QAction *quit_action = new QAction("Exit", m_tray_icon);
    connect(quit_action, SIGNAL(triggered()), this, SLOT(onExit()));
    QAction *show_action = new QAction("Show", m_tray_icon);
    connect(show_action, SIGNAL(triggered()), this, SLOT(show()));
    QAction *settings_action = new QAction("Settings", m_tray_icon);
    connect(settings_action, SIGNAL(triggered()), this, SLOT(on_actionSettings2_triggered()));
    QAction *connect_action = new QAction("Connect", m_tray_icon);
    connect(connect_action, SIGNAL(triggered()), this, SLOT(on_actionConnect_triggered()));
    QAction *disconnect_action = new QAction("Disconnect", m_tray_icon);
    connect(disconnect_action, SIGNAL(triggered()), this, SLOT(on_actionDisconnect_triggered()));
    connect(m_tray_icon, SIGNAL(messageClicked()), this, SLOT(show()));
    connect(m_tray_icon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(show()));
    QMenu *tray_icon_menu = new QMenu;
    tray_icon_menu->addAction(connect_action);
    tray_icon_menu->addAction(disconnect_action);
    tray_icon_menu->addAction(settings_action);
    tray_icon_menu->addAction(show_action);
    tray_icon_menu->addAction(quit_action);
    m_tray_icon->setContextMenu(tray_icon_menu);
    m_tray_icon->show();
}


//utitlity Functions

void MainWindow::closeEvent(QCloseEvent *event)
{
    hide();
    event->ignore();
}

void MainWindow::onExit()
{
    exit(0);
}

//destructor

MainWindow::~MainWindow()
{
    delete ui;
}
