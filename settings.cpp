#include "settings.h"
#include "ui_settings.h"
#include "mainwindow.h"
#include <QColorDialog>
#include <QFileDialog>
#include <QMessageBox>

settings::settings(QString nickName, int port, QString serverAddr, QString chatColor, QString serverColor, QString statusColor, bool proxy, QString proxySrv, int proxyPort, QString proxyUser, QString proxyPass, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::settings)
{
    ui->setupUi(this);
    ui->serverAddr->setText(serverAddr);
    ui->portNum->setMaximum(65535);
    ui->portNum->setValue(port);
    ui->nickName->setText(nickName);
    ui->chatColorLine->setText(chatColor);
    ui->serverColorLine->setText(serverColor);
    ui->statusColorLine->setText(statusColor);
    ui->socksCheck->setChecked(proxy);
    ui->proxySrv->setText(proxySrv);
    ui->proxyPort->setValue(proxyPort);
    ui->proxyUser->setText(proxyUser);
    ui->proxyPass->setText(proxyPass);

}

//on accept

void settings::on_buttonBox_accepted()
{
    emit valueChanged(ui->nickName->text(),
                      ui->serverAddr->text(),
                      ui->portNum->text().toInt(),
                      ui->chatColorLine->text(),
                      ui->serverColorLine->text(),
                      ui->statusColorLine->text(),
                      ui->socksCheck->isChecked(),
                      ui->proxySrv->text(),
                      ui->proxyPort->value(),
                      ui->proxyUser->text(),
                      ui->proxyPass->text()
                     );
}

//chat colors settings

void settings::on_chatPicker_clicked()
{
    QColorDialog *colorDialog = new QColorDialog(this);
    colorDialog->show();
    colorDialog->setCurrentColor(QColor(ui->chatColorLine->text()));
    QObject::connect(colorDialog, SIGNAL(colorSelected(QColor)), this, SLOT(chatSelected(QColor)));
}

void settings::chatSelected(QColor color)
{
    ui->chatColorLine->setText(color.name());
}

void settings::on_serverPicker_clicked()
{
    QColorDialog *colorDialog = new QColorDialog(this);
    colorDialog->show();
    colorDialog->setCurrentColor(QColor(ui->serverColorLine->text()));
    QObject::connect(colorDialog, SIGNAL(colorSelected(QColor)), this, SLOT(serverSelected(QColor)));
}

void settings::serverSelected(QColor color)
{
    ui->serverColorLine->setText(color.name());
}

void settings::on_statusPicker_clicked()
{
    QColorDialog *colorDialog = new QColorDialog(this);
    colorDialog->show();
    colorDialog->setCurrentColor(QColor(ui->statusColorLine->text()));
    QObject::connect(colorDialog, SIGNAL(colorSelected(QColor)), this, SLOT(statusSelected(QColor)));
}

void settings::statusSelected(QColor color)
{
    ui->statusColorLine->setText(color.name());
}

//proxy settings

void settings::on_socksCheck_toggled(bool checked)
{
    ui->proxyPass->setEnabled(checked);
    ui->proxyUser->setEnabled(checked);
    ui->proxyPort->setEnabled(checked);
    ui->proxySrv->setEnabled(checked);
}

//load/save config file

void settings::on_saveCfg_clicked()
{
    QHash<QString, QVariant> settings;
    settings.insert("nickName", ui->nickName->text());
    settings.insert("serverAddr", ui->serverAddr->text());
    settings.insert("portNum", ui->portNum->text());
    settings.insert("chatColor", ui->chatColorLine->text());
    settings.insert("serverColor", ui->serverColorLine->text());
    settings.insert("statusColor", ui->statusColorLine->text());
    settings.insert("socksCheck", ui->socksCheck->isChecked());
    settings.insert("proxySrv", ui->proxySrv->text());
    settings.insert("proxyPort", ui->proxyPort->value());
    settings.insert("proxyUser", ui->proxyUser->text());
    settings.insert("proxyPass", ui->proxyPass->text());

    QString fileName = QFileDialog::getSaveFileName(NULL, tr("Save file"), "client.cfg");
    if (fileName != "") {
        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly)) {
            QDataStream out(&file);
            out << settings;
            file.flush();
            file.close();
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}


void settings::on_loadCfg_clicked()
{
    QFileDialog *f = new QFileDialog();
    QString file = f->getOpenFileName();
    if (file != "") {
        QFile *cfgF = new QFile(file);
        if (cfgF->open(QIODevice::ReadOnly)) {
            QDataStream in(cfgF);
            QHash<QString, QVariant> settings;
            in >> settings;
            ui->nickName->setText(settings["nickName"].toString());
            ui->serverAddr->setText(settings["serverAddr"].toString());
            ui->portNum->setValue(settings["portNum"].toInt());
            ui->chatColorLine->setText(settings["chatColor"].toString());
            ui->serverColorLine->setText(settings["serverColor"].toString());
            ui->statusColorLine->setText(settings["statusColor"].toString());
            ui->socksCheck->setChecked(settings["socksCheck"].toBool());
            ui->proxySrv->setText(settings["proxySrv"].toString());
            ui->proxyPort->setValue(settings["proxyPort"].toInt());
            ui->proxyUser->setText(settings["proxyUser"].toString());
            ui->proxyPass->setText(settings["proxyPass"].toString());
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

//destructor

settings::~settings()
{
    delete ui;
}
