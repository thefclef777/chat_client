#include "basic.h"
#include "ui_basic.h"

Basic::Basic(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Basic)
{
    ui->setupUi(this);
    this->setWindowTitle("Basic");
}

Basic::~Basic()
{
    delete ui;
}
