#include "advanced.h"
#include "ui_advanced.h"
#include "crypto.h"
#include "QFileDialog"
#include <QMessageBox>

Advanced::Advanced(crypto *crypt, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Advanced)
{
    ui->setupUi(this);
    this->setWindowTitle("Advanced");
    this->crypt = crypt;
    ui->checkBox_2->setChecked(crypt->getAES());
    ui->DEScheckBox->setChecked(crypt->getDES());
    ui->BLowcheckBox_3->setChecked(crypt->getBlowfish());
    ui->TwocheckBox_4->setChecked(crypt->getTwofish());
    ui->SerpcheckBox_5->setChecked(crypt->getSerpent());
}

// AES Functions

void Advanced::on_pushButton_2_clicked()
{
    crypt->AESGen();
}

void Advanced::on_toolButton_2_clicked()
{
    QFileDialog *f = new QFileDialog();
    QString file = f->getOpenFileName();
    if (file != "") {
        QFile *keyF = new QFile(file);
        QByteArray ba;
        if (keyF->open(QIODevice::ReadOnly)) {
            ba = keyF->readAll();
            crypt->setAESKey(ba);
            ui->lineEdit_2->setText(file);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

void Advanced::on_AESSave_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(NULL, tr("Save file"), "key.aes");
    if (fileName != "") {
        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly)) {
            file.write(crypt->getAESKey());
            file.close();
            ui->lineEdit_2->setText(fileName);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

void Advanced::on_checkBox_2_toggled(bool checked)
{
    crypt->useAES(checked);
    ui->lineEdit_2->setEnabled(checked);
}

//DES functions

void Advanced::on_DEScheckBox_toggled(bool checked)
{
    crypt->useDES(checked);
    ui->DESlineEdit->setEnabled(checked);
}

void Advanced::on_DESpushButton_clicked()
{
    crypt->DESGen();
}

void Advanced::on_DESSave_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(NULL, tr("Save file"), "key.des");
    if (fileName != "") {
        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly)) {
            file.write(crypt->getDESKey());
            file.close();
            ui->DESlineEdit->setText(fileName);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

void Advanced::on_DEStoolButton_clicked()
{
    QFileDialog *f = new QFileDialog();
    QString file = f->getOpenFileName();
    if (file != "") {
        QFile *keyF = new QFile(file);
        QByteArray ba;
        if (keyF->open(QIODevice::ReadOnly)) {
            ba = keyF->readAll();
            crypt->setDESKey(ba);
            ui->DESlineEdit->setText(file);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

//Blowfish functions

void Advanced::on_BLowcheckBox_3_toggled(bool checked)
{
    crypt->useBlowfish(checked);
    ui->BlowlineEdit_3->setEnabled(checked);
}

void Advanced::on_BlowpushButton_3_clicked()
{
    crypt->BlowfishGen();
}

void Advanced::on_BlowpushButton_12_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(NULL, tr("Save file"), "key.blowfish");
    if (fileName != "") {
        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly)) {
            file.write(crypt->getBlowfishKey());
            file.close();
            ui->BlowlineEdit_3->setText(fileName);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

void Advanced::on_BlowtoolButton_3_clicked()
{
    QFileDialog *f = new QFileDialog();
    QString file = f->getOpenFileName();
    if (file != "") {
        QFile *keyF = new QFile(file);
        QByteArray ba;
        if (keyF->open(QIODevice::ReadOnly)) {
            ba = keyF->readAll();
            crypt->setBlowfishKey(ba);
            ui->BlowlineEdit_3->setText(file);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

//TwoFish Functions

void Advanced::on_TwocheckBox_4_toggled(bool checked)
{
    crypt->useTwofish(checked);
    ui->TwolineEdit_4->setEnabled(checked);
}

void Advanced::on_TwopushButton_4_clicked()
{
    crypt->TwofishGen();
}

void Advanced::on_TwopushButton_13_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(NULL, tr("Save file"), "key.twofish");
    if (fileName != "") {
        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly)) {
            file.write(crypt->getTwofishKey());
            file.close();
            ui->TwolineEdit_4->setText(fileName);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

void Advanced::on_TwotoolButton_4_clicked()
{
    QFileDialog *f = new QFileDialog();
    QString file = f->getOpenFileName();
    if (file != "") {
        QFile *keyF = new QFile(file);
        QByteArray ba;
        if (keyF->open(QIODevice::ReadOnly)) {
            ba = keyF->readAll();
            crypt->setTwofishKey(ba);
            ui->TwolineEdit_4->setText(file);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

//Serpent Functions

void Advanced::on_SerpcheckBox_5_toggled(bool checked)
{
    crypt->useSerpent(checked);
    ui->SerplineEdit_5->setEnabled(checked);
}

void Advanced::on_SerppushButton_5_clicked()
{
    crypt->SerpentGen();
}

void Advanced::on_SerppushButton_14_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(NULL, tr("Save file"), "key.serpent");
    if (fileName != "") {
        QFile file(fileName);
        if (file.open(QIODevice::WriteOnly)) {
            file.write(crypt->getSerpentKey());
            file.close();
            ui->SerplineEdit_5->setText(fileName);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

void Advanced::on_SerptoolButton_5_clicked()
{
    QFileDialog *f = new QFileDialog();
    QString file = f->getOpenFileName();
    if (file != "") {
        QFile *keyF = new QFile(file);
        QByteArray ba;
        if (keyF->open(QIODevice::ReadOnly)) {
            ba = keyF->readAll();
            crypt->setSerpentKey(ba);
            ui->SerplineEdit_5->setText(file);
        } else {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", "Could not open file!");
            messageBox.setFixedSize(500, 200);
        }
    }
}

//destructor

Advanced::~Advanced()
{
    delete ui;
}
