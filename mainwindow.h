#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "client.h"
#include "crypto.h"
#include <QMainWindow>
#include <QSystemTrayIcon>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void SendMsg(QString);
    Client *client;
    ~MainWindow();
    void create_tray_icon();
    QSystemTrayIcon * m_tray_icon;
    void closeEvent(QCloseEvent *event);



public slots:
    void setServer(QString, int);
    void display(QByteArray);
    void connected();
    void notConnected();
    void showMessage(QString);
    void sendURL(QString,QString);
    void valueChanged(QString,QString,int,QString,QString,QString,bool, QString, int, QString, QString);

private slots:
    void on_sendButton_clicked();
    void on_actionSettings2_triggered();
    void on_actionConnect_triggered();
    void on_actionBasic_triggered();
    void on_actionFile_triggered();
    void on_actionAdvanced_triggered();
    void on_actionURL_triggered();
    void on_actionDisconnect_triggered();
    void on_actionImport_Keys_triggered();
    void on_actionExport_Keys_triggered();
    void onExit();

private:
    Ui::MainWindow *ui;
    int portNum;
    QString nickName;
    QString serverAddr;
    QString textColor;
    QString serverColor;
    QString statusColor;
    crypto *crypt;
    bool useProxy;
};

#endif // MAINWINDOW_H
