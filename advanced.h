#ifndef ADVANCED_H
#define ADVANCED_H
#include "crypto.h"

#include <QDialog>

namespace Ui {
class Advanced;
}

class Advanced : public QDialog
{
    Q_OBJECT

public:
    explicit Advanced(crypto*, QWidget *parent = 0);
    ~Advanced();

private slots:
    void on_pushButton_2_clicked();
    void on_toolButton_2_clicked();
    void on_AESSave_clicked();
    void on_checkBox_2_toggled(bool checked);
    void on_DEScheckBox_toggled(bool checked);
    void on_DESpushButton_clicked();
    void on_DESSave_clicked();
    void on_DEStoolButton_clicked();
    void on_BLowcheckBox_3_toggled(bool checked);
    void on_BlowpushButton_3_clicked();
    void on_BlowpushButton_12_clicked();
    void on_BlowtoolButton_3_clicked();
    void on_TwocheckBox_4_toggled(bool checked);
    void on_TwopushButton_4_clicked();
    void on_TwopushButton_13_clicked();
    void on_TwotoolButton_4_clicked();
    void on_SerpcheckBox_5_toggled(bool checked);
    void on_SerppushButton_5_clicked();
    void on_SerppushButton_14_clicked();
    void on_SerptoolButton_5_clicked();

private:
    Ui::Advanced *ui;
    crypto * crypt;
};

#endif // ADVANCED_H
