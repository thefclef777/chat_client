#ifndef CLEINT
#define CLEINT

#include <QtCore>
#include <QtNetwork>

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QObject *parent = 0);
    QString checkData();
    QString getErrorCode();
    void setProxy(QNetworkProxy);
    QNetworkProxy getProxy();
    void disableProxy();
    ~Client();

public slots:
    void readyRead();
    bool connectToHost(QString host, int port);
    bool writeData(QByteArray data);
    void disconnectServer();
    void timeout();
    void socketConnected();

signals:
    void dataReceived(QByteArray);
    void notConnected();
    void connected();

private:
    QTcpSocket *socket;
    QTimer timer;
    QByteArray *buffer;
    quint64 *s;
    quint64 size;
};

#endif // CLEINT

