#ifndef BASIC_H
#define BASIC_H

#include <QDialog>

namespace Ui {
class Basic;
}

class Basic : public QDialog
{
    Q_OBJECT

public:
    explicit Basic(QWidget *parent = 0);
    ~Basic();

private:
    Ui::Basic *ui;
};

#endif // BASIC_H
