#include "client.h"

static inline QByteArray IntToArray(quint64 source);
static inline quint64 ArrayToInt(QByteArray source);

Client::Client(QObject *parent) : QObject(parent)
{
    socket = new QTcpSocket(this);
    QObject::connect(socket, SIGNAL(readyRead()), SLOT(readyRead()));
    buffer = new QByteArray();
    s = new quint64(0);
    size = *s;
}

//connection functions

bool Client::connectToHost(QString host, int port)
{
    socket->abort();
    socket->disconnectFromHost();
    socket->connectToHost(host, port);
    QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(timeout()));
    QObject::connect(socket, SIGNAL(connected()), this, SLOT(socketConnected()));
    QObject::connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(timeout()));
    timer.start(3000);
    return true;
}
void Client::socketConnected()
{
    timer.stop();
    QObject::disconnect(socket, SIGNAL(connected()), this, SLOT(socketConnected()));
    QObject::disconnect(&timer, SIGNAL(timeout()), this, SLOT(timeout()));
    QObject::disconnect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(timeout()));
    emit connected();
}

void Client::timeout()
{
    socket->disconnectFromHost();
    socket->abort();
    QObject::disconnect(socket, SIGNAL(connected()), this, SLOT(socketConnected()));
    QObject::disconnect(&timer, SIGNAL(timeout()), this, SLOT(timeout()));
    QObject::disconnect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(timeout()));

    timer.stop();
    emit notConnected();
}

void Client::disconnectServer()
{
    socket->disconnectFromHost();
}

QString Client::getErrorCode()
{
    return socket->errorString();
}

//RX TX Functions

bool Client::writeData(QByteArray data)
{
    if (socket->state() == QAbstractSocket::ConnectedState) {
        socket->write(IntToArray(data.size()));
        socket->write(data);
        return socket->waitForBytesWritten();
    } else
        return false;
}

void Client::readyRead()
{
    while (socket->bytesAvailable() > 0) {
        buffer->append(socket->readAll());
        while ((size == 0 && (quint64) buffer->size() >= 8) || (size > 0 && (quint64) buffer->size() >= size)) {
            if (size == 0 && buffer->size() >= 8) {
                size = ArrayToInt(buffer->mid(0, 8));
                *s = size;
                buffer->remove(0, 8);
            }
            if (size > 0 && (quint64) buffer->size() >= size) {
                QByteArray data = buffer->mid(0, size);
                buffer->remove(0, size);
                size = 0;
                *s = size;
                emit dataReceived(data);
            }
        }
    }
}

//Proxy Settings

void Client::setProxy(QNetworkProxy proxy)
{
    socket->setProxy(proxy);
}

void Client::disableProxy()
{
    socket->setProxy(QNetworkProxy::NoProxy);
    socket->disconnectFromHost();
    socket->close();
}

QNetworkProxy Client::getProxy()
{
    return socket->proxy();
}


//utility functions

QByteArray IntToArray(quint64 source)
{
    QByteArray temp;
    QDataStream data(&temp, QIODevice::ReadWrite);
    data << source;
    return temp;
}

quint64 ArrayToInt(QByteArray source)
{
    quint64 temp;
    QDataStream data(&source, QIODevice::ReadWrite);
    data >> temp;
    return temp;
}

QString Client::checkData()
{
    QString data;
    if (data != "" && data != NULL) {
        return data;
    }
    return "";
}

//destructor

Client::~Client()
{
    disconnectServer();
    delete socket;
}
