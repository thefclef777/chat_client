#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include "mainwindow.h"
#include "client.h"
namespace Ui {
class settings;
}

class settings : public QDialog
{
    Q_OBJECT

public:
    explicit settings(QString nickName = "",int port=0, QString serverAddr="", QString = "",QString = "",QString ="", bool proxy=false, QString proxySrv="", int proxyPort=0, QString proxyUser = "", QString proxyPass = "", QWidget *parent = 0);
    ~settings();
public slots:
    void chatSelected(QColor);
    void serverSelected(QColor);
    void statusSelected(QColor);

signals:
    void valueChanged(QString,QString, int, QString,QString,QString, bool, QString, int, QString, QString);

private slots:
    void on_buttonBox_accepted();
    void on_chatPicker_clicked();
    void on_serverPicker_clicked();
    void on_statusPicker_clicked();
    void on_socksCheck_toggled(bool checked);
    void on_saveCfg_clicked();
    void on_loadCfg_clicked();

private:
    Ui::settings *ui;
};

#endif // SETTINGS_H
