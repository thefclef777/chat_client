#include "urldialog.h"
#include "ui_urldialog.h"

URLDialog::URLDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::URLDialog)
{
    ui->setupUi(this);
    connect(this,SIGNAL(accepted()),this,SLOT(sendInfo()));
}

void URLDialog::sendInfo()
{
    emit sendURL(ui->Text->text(),ui->URL->text());
}

URLDialog::~URLDialog()
{
    delete ui;
}
