#include "crypto.h"

crypto::crypto(QObject *parent) : QObject(parent)
{
    AutoSeededRandomPool prng;
    AESkey = SecByteBlock(AES::DEFAULT_KEYLENGTH);
    AESGen();
    DESkey = SecByteBlock(DES_EDE3::DEFAULT_KEYLENGTH);
    DESGen();
    BlowfishKey = SecByteBlock(Blowfish::MAX_KEYLENGTH);
    BlowfishGen();
    TwofishKey = SecByteBlock(Twofish::DEFAULT_KEYLENGTH);
    TwofishGen();
    SerpentKey = SecByteBlock(Serpent::DEFAULT_KEYLENGTH);
    SerpentGen();
    AESbool = false;
    DESbool = false;
    Blowfishbool = false;
    Twofishbool = false;
    Serpentbool = false;
}

//encrypt/decrypt functions

QByteArray crypto::toEnc(QByteArray data)
{
    if (AESbool) {
        data = AESE(data);
    }
    if (DESbool) {
        data = DESE(data);
    }
    if (Blowfishbool) {
        data = BlowfishE(data);
    }
    if (Twofishbool) {
        data = TwofishE(data);
    }
    if (Serpentbool) {
        data = SerpentE(data);
    }
    return data;
}

QByteArray crypto::toDec(QByteArray data)
{
    if (Serpentbool) {
        data = SerpentD(data);
    }
    if (Twofishbool) {
        data = TwofishD(data);
    }
    if (Blowfishbool) {
        data = BlowfishD(data);
    }
    if (DESbool) {
        data = DESD(data);
    }
    if (AESbool) {
        data = AESD(data);
    }
    return data;
}


//get specific key

QByteArray crypto::getAESKey()
{
    return QByteArray::fromStdString(string((char *)AESkey.data(), AESkey.size()));
}

QByteArray crypto::getDESKey()
{
    return QByteArray::fromStdString(string((char *)DESkey.data(), DESkey.size()));
}

QByteArray crypto::getBlowfishKey()
{
    return QByteArray::fromStdString(string((char *)BlowfishKey.data(), BlowfishKey.size()));
}

QByteArray crypto::getTwofishKey()
{
    return QByteArray::fromStdString(string((char *)TwofishKey.data(), TwofishKey.size()));
}


QByteArray crypto::getSerpentKey()
{
    return QByteArray::fromStdString(string((char *)SerpentKey.data(), SerpentKey.size()));
}

//get if we use

bool crypto::getAES()
{
    return AESbool;
}

bool crypto::getDES()
{
    return DESbool;
}

bool crypto::getBlowfish()
{
    return Blowfishbool;
}

bool crypto::getTwofish()
{
    return Twofishbool;
}

bool crypto::getSerpent()
{
    return Serpentbool;
}
//set Keys

void crypto::setAESKey(QByteArray key)
{
    string temp = key.toStdString();
    AESkey = SecByteBlock((const unsigned char *)temp.data(), temp.size());
}

void crypto::setDESKey(QByteArray key)
{
    string temp = key.toStdString();
    DESkey = SecByteBlock((const unsigned char *)temp.data(), temp.size());
}

void crypto::setBlowfishKey(QByteArray key)
{
    string temp = key.toStdString();
    BlowfishKey = SecByteBlock((const unsigned char *)temp.data(), temp.size());
}

void crypto::setTwofishKey(QByteArray key)
{
    string temp = key.toStdString();
    TwofishKey = SecByteBlock((const unsigned char *)temp.data(), temp.size());
}

void crypto::setSerpentKey(QByteArray key)
{
    string temp = key.toStdString();
    SerpentKey = SecByteBlock((const unsigned char *)temp.data(), temp.size());
}

//key generators

void crypto::AESGen()
{
    prng.GenerateBlock(AESkey, AESkey.size());
}

void crypto::DESGen()
{
    prng.GenerateBlock(DESkey, DESkey.size());
}

void crypto::BlowfishGen()
{
    prng.GenerateBlock(BlowfishKey, BlowfishKey.size());
}

void crypto::TwofishGen()
{
    prng.GenerateBlock(TwofishKey, TwofishKey.size());
}

void crypto::SerpentGen()
{
    prng.GenerateBlock(SerpentKey, SerpentKey.size());
}

//set if we use

void crypto::useAES(bool set)
{
    AESbool = set;
}

void crypto::useDES(bool set)
{
    DESbool = set;
}

void crypto::useBlowfish(bool set)
{
    Blowfishbool = set;
}

void crypto::useTwofish(bool set)
{
    Twofishbool = set;
}

void crypto::useSerpent(bool set)
{
    Serpentbool = set;
}

//Specific encryption functions

QByteArray crypto::AESE(QByteArray data)
{
    byte iv[AES::BLOCKSIZE];
    prng.GenerateBlock(iv, sizeof(iv));
    string plain = data.toStdString();
    string cipher;
    try {
        CTR_Mode< AES >::Encryption e;
        e.SetKeyWithIV(AESkey, AESkey.size(), iv);
        StringSource(plain, true,
                     new StreamTransformationFilter(e,
                             new StringSink(cipher)
                                                   )
                    );
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
    }
    return QByteArray((const char *)iv, sizeof(iv)).append(QByteArray::fromStdString(cipher));
}

QByteArray crypto::DESE(QByteArray data)
{
    byte iv[DES_EDE3::BLOCKSIZE];
    prng.GenerateBlock(iv, sizeof(iv));
    string plain = data.toStdString();
    string cipher;
    try {
        CBC_Mode< DES_EDE3 >::Encryption e;
        e.SetKeyWithIV(DESkey, DESkey.size(), iv);
        StringSource(plain, true,
                     new StreamTransformationFilter(e,
                             new StringSink(cipher)
                                                   )
                    );
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
    }
    return QByteArray((const char *)iv, sizeof(iv)).append(QByteArray::fromStdString(cipher));
}

QByteArray crypto::BlowfishE(QByteArray data)
{
    byte iv[Blowfish::BLOCKSIZE];
    prng.GenerateBlock(iv, sizeof(iv));
    string plain = data.toStdString();
    string cipher;
    try {
        CTR_Mode< Blowfish >::Encryption e;
        e.SetKeyWithIV(BlowfishKey, BlowfishKey.size(), iv);
        StringSource(plain, true,
                     new StreamTransformationFilter(e,
                             new StringSink(cipher)
                                                   )
                    );
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
    }
    return QByteArray((const char *)iv, sizeof(iv)).append(QByteArray::fromStdString(cipher));
}

QByteArray crypto::TwofishE(QByteArray data)
{
    byte iv[Twofish::BLOCKSIZE];
    prng.GenerateBlock(iv, sizeof(iv));
    string plain = data.toStdString();
    string cipher;
    try {
        CTR_Mode< Twofish >::Encryption e;
        e.SetKeyWithIV(TwofishKey, TwofishKey.size(), iv);
        StringSource two(plain, true,
                         new StreamTransformationFilter(e,
                                 new StringSink(cipher)
                                                       )
                        );
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
    }
    return QByteArray((const char *)iv, sizeof(iv)).append(QByteArray::fromStdString(cipher));
}

QByteArray crypto::SerpentE(QByteArray data)
{
    byte iv[Serpent::BLOCKSIZE];
    prng.GenerateBlock(iv, sizeof(iv));
    string plain = data.toStdString();
    string cipher;
    try {
        CBC_Mode< Serpent >::Encryption e;
        e.SetKeyWithIV(SerpentKey, SerpentKey.size(), iv);
        StringSource ss12(plain, true,
                          new StreamTransformationFilter(e,
                                  new StringSink(cipher)
                                                        )
                         );
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
    }
    return QByteArray((const char *)iv, sizeof(iv)).append(QByteArray::fromStdString(cipher));


}

//decryption functions

QByteArray crypto::AESD(QByteArray data)
{
    string cipher, recovered;
    CTR_Mode< AES >::Decryption d;
    QByteArray bIV = data.mid(0, 16);
    data.remove(0, 16);
    cipher = data.toStdString();
    byte *iv;
    iv = (byte *)bIV.data();
    try {
        d.SetKeyWithIV(AESkey, AESkey.size(), iv);
        StringSource s(cipher, true,
                       new StreamTransformationFilter(d,
                               new StringSink(recovered)
                                                     )
                      );
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
    }
    return QByteArray::fromStdString(recovered);
}

QByteArray crypto::DESD(QByteArray data)
{
    string cipher, recovered;
    QByteArray bIV = data.mid(0, 8);
    data.remove(0, 8);
    cipher = data.toStdString();
    byte *iv;
    iv = (byte *)bIV.data();
    try {
        CBC_Mode< DES_EDE3 >::Decryption d;
        d.SetKeyWithIV(DESkey, DESkey.size(), iv);
        StringSource s(cipher, true,
                       new StreamTransformationFilter(d,
                               new StringSink(recovered)
                                                     )
                      );
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
    }
    return QByteArray::fromStdString(recovered);
}

QByteArray crypto::BlowfishD(QByteArray data)
{
    string cipher, recovered;
    QByteArray bIV = data.mid(0, 8);
    data.remove(0, 8);
    cipher = data.toStdString();
    byte *iv;
    iv = (byte *)bIV.data();
    try {
        CTR_Mode< Blowfish >::Decryption d;
        d.SetKeyWithIV(BlowfishKey, BlowfishKey.size(), iv);
        StringSource s(cipher, true,
                       new StreamTransformationFilter(d,
                               new StringSink(recovered)
                                                     )
                      );
        return QByteArray::fromStdString(recovered);
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
    }
    return QByteArray::fromStdString(recovered);
}

QByteArray crypto::TwofishD(QByteArray data)
{
    string cipher, recovered;
    QByteArray bIV = data.mid(0, 16);
    data.remove(0, 16);
    cipher = data.toStdString();
    byte *iv;
    iv = (byte *)bIV.data();
    try {
        CTR_Mode< Twofish >::Decryption d;
        d.SetKeyWithIV(TwofishKey, TwofishKey.size(), iv);
        StringSource sz(cipher, true,
                        new StreamTransformationFilter(d,
                                new StringSink(recovered)
                                                      )
                       );
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
    }
    return QByteArray::fromStdString(recovered);
}

QByteArray crypto::SerpentD(QByteArray data)
{
    string cipher, recovered;
    QByteArray bIV = data.mid(0, 16);
    data.remove(0, 16);
    cipher = data.toStdString();
    byte *iv;
    iv = (byte *)bIV.data();
    try {
        CBC_Mode< Serpent >::Decryption d;
        d.SetKeyWithIV(SerpentKey, SerpentKey.size(), iv);
        StringSource ss5(cipher, true,
                         new StreamTransformationFilter(d,
                                 new StringSink(recovered)
                                                       )
                        );
    } catch (const CryptoPP::Exception &e) {
        cerr << e.what() << endl;
        exit(1);
    }
    return QByteArray::fromStdString(recovered);
}

//set and get all keys

QHash<QString, QVariant> crypto::getUsedKeys()
{
    QHash<QString, QVariant> keys;
    if (AESbool) keys.insert("AES", getAESKey());
    if (DESbool) keys.insert("DES", getDESKey());
    if (Blowfishbool) keys.insert("Blowfish", getBlowfishKey());
    if (Twofishbool) keys.insert("Twofish", getTwofishKey());
    if (Serpentbool) keys.insert("Serpent", getSerpentKey());
    return keys;
}

void crypto::setUsedKeys(QHash<QString, QVariant> keyset)
{
    if (keyset.contains("AES")) {
        setAESKey(keyset["AES"].toByteArray());
        AESbool = true;
    }
    if (keyset.contains("DES")) {
        setDESKey(keyset["DES"].toByteArray());
        DESbool = true;
    }
    if (keyset.contains("Blowfish")) {
        setBlowfishKey(keyset["Blowfish"].toByteArray());
        Blowfishbool = true;
    }
    if (keyset.contains("Twofish")) {
        setTwofishKey(keyset["Twofish"].toByteArray());
        Twofishbool = true;
    }
    if (keyset.contains("Serpent")) {
        setSerpentKey(keyset["Serpent"].toByteArray());
        Serpentbool = true;
    }
}

//destructor

crypto::~crypto()
{

}
