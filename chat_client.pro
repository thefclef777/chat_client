#-------------------------------------------------
#
# Project created by QtCreator 2015-08-22T00:46:55
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = chat_client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    client.cpp \
    settings.cpp \
    basic.cpp \
    advanced.cpp \
    urldialog.cpp \
    crypto.cpp

HEADERS  += mainwindow.h \
    client.h \
    settings.h \
    basic.h \
    advanced.h \
    urldialog.h \
    crypto.h

FORMS    += mainwindow.ui \
    settings.ui \
    basic.ui \
    advanced.ui \
    urldialog.ui

LIBS += -L/usr/include/cryptopp -lcryptopp

DISTFILES += \
    client.png

