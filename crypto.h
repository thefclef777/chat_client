#ifndef CRYPTO_H
#define CRYPTO_H

#include <QObject>
#include <iostream>
#include <iomanip>
#include <string>
#include <QByteArray>
#include <QDebug>
#include "crypto.h"
#include "cryptopp/cryptlib.h"
#include "cryptopp/aes.h"
#include "cryptopp/ccm.h"
#include "cryptopp/cbcmac.h"
#include "cryptopp/modes.h"
#include "cryptopp/filters.h"
#include "cryptopp/cryptlib.h"
#include "cryptopp/randpool.h"
#include "cryptopp/osrng.h"
#include "cryptopp/rijndael.h"
#include "cryptopp/hex.h"
#include "cryptopp/secblock.h"
#include "cryptopp/modes.h"
#include "cryptopp/des.h"
#include "cryptopp/blowfish.h"
#include "cryptopp/twofish.h"
#include "cryptopp/serpent.h"

using CryptoPP::Twofish;
using CryptoPP::Serpent;
using CryptoPP::Blowfish;
using CryptoPP::DES_EDE3;
using CryptoPP::CBC_Mode;
using CryptoPP::SecByteBlock;
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;
using CryptoPP::AutoSeededRandomPool;
using CryptoPP::Exception;
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;
using CryptoPP::AES;
using CryptoPP::CTR_Mode;
using std::string;
using std::cout;
using std::cerr;
using std::endl;

class crypto : public QObject
{
    Q_OBJECT

public:
    explicit crypto(QObject *parent = 0);
    ~crypto();
    QByteArray toEnc(QByteArray data);
    QByteArray toDec(QByteArray data);
    void AESGen();
    void DESGen();
    void BlowfishGen();
    void TwofishGen();
    void SerpentGen();
    void setDESKey(QByteArray key);
    void setAESKey(QByteArray key);
    void setBlowfishKey(QByteArray key);
    void setTwofishKey(QByteArray key);
    void setSerpentKey(QByteArray key);
    QByteArray getAESKey();
    QByteArray getDESKey();
    QByteArray getBlowfishKey();
    QByteArray getTwofishKey();
    QByteArray getSerpentKey();
    void useAES(bool);
    void useDES(bool);
    void useBlowfish(bool);
    void useTwofish(bool);
    void useSerpent(bool);
    bool getAES();
    bool getDES();
    bool getBlowfish();
    bool getTwofish();
    bool getSerpent();
    QHash<QString,QVariant> getUsedKeys();
    void setUsedKeys(QHash<QString,QVariant>);

public slots:

private:
    AutoSeededRandomPool prng;
    bool AESbool;
    bool DESbool;
    bool Blowfishbool;
    bool Twofishbool;
    bool Serpentbool;
    SecByteBlock AESkey;
    SecByteBlock DESkey;
    SecByteBlock BlowfishKey;
    SecByteBlock TwofishKey;
    SecByteBlock SerpentKey;
    QByteArray AESE(QByteArray);
    QByteArray DESE(QByteArray);
    QByteArray BlowfishE(QByteArray);
    QByteArray TwofishE(QByteArray);
    QByteArray SerpentE(QByteArray);
    QByteArray AESD(QByteArray);
    QByteArray DESD(QByteArray);
    QByteArray BlowfishD(QByteArray);
    QByteArray TwofishD(QByteArray);
    QByteArray SerpentD(QByteArray);
};

#endif // CRYPTO_H
