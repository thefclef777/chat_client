# README #

A basic crytpo-chat application. Allows users to send and receive using 
a variety of algorithms.

### What is this repository for? ###

Main repo for the chat client.

### How do I get set up? ###

Linux: Requires Crypto++ lib and Qt5 to compile.
Windows: Requires Crypto++ lib installed and configured correctly in .pro file. Will post a multi-platform .pro file later. 

Builds are available at http://www.thefclef777.com/me/projects.php